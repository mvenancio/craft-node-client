module.exports = function (app) {
  var api = app.controllers.stuffApi;
  app.get("/api/stuffs", api.get);
  app.get("/api/stuffs/:id", api.get);
  app.post("/api/stuffs", api.post);
  app.put("/api/stuffs", api.put);
};