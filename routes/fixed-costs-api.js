module.exports = function (app) {
  var api = app.controllers.fixedCostsApi;
  app.get("/api/fixed-costs", api.get);
  app.get("/api/fixed-costs/:id", api.getBy);
  app.get("/api/fixed-costs/total", api.getTotal);
  app.post("/api/fixed-costs", api.post);
  app.put("/api/fixed-costs", api.put);
};
