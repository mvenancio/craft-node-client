module.exports = function (app) {
  var preferences = app.controllers.preferencesApi;
  app.get("/api/preferences", preferences.get);
  app.post("/api/preferences", preferences.post);
  app.put("/api/preferences", preferences.put);
  app.post("/api/upload", preferences.upload);
};