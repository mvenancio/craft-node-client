module.exports = function (app) {
  var api = app.controllers.supplierApi;
  app.get("/api/suppliers", api.get);
  app.post("/api/suppliers", api.post);
  app.put("/api/suppliers", api.put);
};