module.exports = function (app) {
  var api = app.controllers.sellerApi;
  app.get("/api/sellers", api.get);
  app.get("/api/sellers/:id", api.get);
  app.post("/api/sellers", api.post);
  app.put("/api/sellers", api.put);
};