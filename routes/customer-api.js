module.exports = function (app) {
  var api = app.controllers.customerApi;
  app.get("/api/customers", api.get);
  app.get("/api/customers/:id", api.get);
  app.post("/api/customers", api.post);
  app.put("/api/customers", api.put);
};