module.exports = function (app) {
  var api = app.controllers.signatureApi;
  app.get("/api/signatures", api.get);
  app.post("/api/signatures", api.post);
  app.delete("/api/signatures", api.delete);
};