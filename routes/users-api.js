module.exports = function (app) {
  var api = app.controllers.usersApi;
  app.get("/api/users", api.get);
  app.post("/api/users", api.post);
  app.put("/api/users", api.put);
  app.get("/api/users/remember", api.sendEmail);
};