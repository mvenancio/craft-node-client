module.exports = function (app) {
  var api = app.controllers.depreciationCostsApi;
  app.get("/api/depreciation-costs", api.get);
  app.get("/api/depreciation-costs/:id", api.get);
  app.get("/api/depreciation-costs/total", api.getTotal);
  app.post("/api/depreciation-costs", api.post);
  app.put("/api/depreciation-costs", api.put);
};