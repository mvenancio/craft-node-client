module.exports = function (app) {
  var api = app.controllers.paymentApi;
  app.get("/api/payments", api.get);
  app.get("/api/payments/expired", api.getExpired);
  app.post("/api/payments/pay", api.pay);
  app.post("/api/payments/cancel", api.cancel);
  app.get("/api/payments/total-paid", api.totalPaid);
};