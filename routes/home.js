module.exports = function (app) {
  var home = app.controllers.home;
  app.get("/",home.index);
  app.get("/register",home.register);
  app.get("/system",home.system);
  app.get("/main",home.home);
  app.get("/preferences",home.preferences);
  app.get("/logout",home.logout);
  app.post("/login",home.login);
};