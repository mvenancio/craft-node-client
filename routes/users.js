module.exports = function (app) {
  var users = app.controllers.users;
  app.get("/users", users.index);
  app.get("/new-user", users.newUser);
  app.get("/authorize", users.authorize);
  app.get("/forgot", users.forgot);
  app.post("/update-password", users.updatePassword);
  app.post("/reset-password", users.resetPassword);
};