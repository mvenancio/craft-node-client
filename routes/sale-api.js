module.exports = function (app) {
  var api = app.controllers.saleApi;
  app.get("/api/sales", api.get);
  app.get("/api/sales/:id", api.getBy);
  app.get("/api/sales/:id/payments", api.getPaymentsFromSale);
  app.post("/api/sales", api.post);
  app.put("/api/sales", api.put);
  app.post("/api/sales/approve", api.approve);
  app.post("/api/sales/cancel", api.cancel);
  app.get("/api/sales/finisheds/summary", api.getFinishedsSummary);
};