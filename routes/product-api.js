module.exports = function (app) {
  var api = app.controllers.productApi;
  app.get("/api/products", api.get);
  app.get("/api/products/:id", api.get);
  app.post("/api/products", api.post);
  app.put("/api/products", api.put);
};