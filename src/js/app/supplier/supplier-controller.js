(function () {
    'use strict';
    var app = angular.module('supplier-app', []),
        host = '/api/suppliers',
        config = {
          data: '',
          headers: {
            "Content-Type": "application/json;charset=UTF-8",
            "Accept": "application/json;charset=UTF-8"
          }
        };

        app.controller('supplierController', function ($scope, $http) {
          $scope.load = function () {
              $scope.loading = true;
              $http.get(host, config).success( function (response) {
                 $scope.suppliers = response.items;
              }).finally(function () {
                 $scope.loading = false;
              });
          };

          $scope.reset = function () {
            delete $scope.supplier;
            $scope.new = false;
            $scope.editing = false;
          };

          $scope.save = function (supplier) {
            if (supplier.id) {
              $http.put(host, supplier, config)
                .error(function (response) {
                   console.log("Ocorreu erro ao atualizar");
                })
                .success(function (data) {
                  $scope.load();
                })
            } else {
              $http.post(host, supplier, config)
                .error(function (response) {
                   console.log("Ocorreu erro ao criar");
                })
                .success(function (data) {
                  $scope.load();
                });
            }
            $scope.reset();
          };

          $scope.createNew = function () {
            $scope.new = true;
            $('#name').focus();
          };

          $scope.edit = function (supplier) {
            $scope.supplier = supplier;
            $scope.editing = true;
            $('#name').focus();
          };

          $scope.cancel= function () {
            $scope.reset();
          };

          $scope.load();
        });
}());
