angular.module("craft-app").service("signatureService", function ($http) {
	this.get = function () {
		return $http.get("/api/signatures");
	}
	this.create = function () {
		return $http.post("/api/signatures");
	}
	this.delete = function () {
		return $http.delete("/api/signatures");
	}
})