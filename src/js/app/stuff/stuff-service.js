angular.module("craft-app").service("stuffServiceAPI", function ($http) {
	this.get = function () {
		return $http.get("/api/stuffs");
	}
	this.load = function (id) {
		return $http.get("/api/stuffs/" + id);
	}
	this.update = function (stuff) {
		return $http.put("/api/stuffs", stuff);
	}
	this.create = function (stuff) {
		return $http.post("/api/stuffs", stuff);
	}
})