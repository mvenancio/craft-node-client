angular.module("craft-app").controller("StuffFormController", 
		function ($scope, $location, $routeParams, util, stuffServiceAPI) {

	if ($routeParams.id) {
		stuffServiceAPI.load($routeParams.id).success( function (response) {
			$scope.stuff = response;
		});
	}
	$scope.save = function (stuff) {
		if (stuff.id) {
      stuffServiceAPI.update(stuff).error(function (response) {
        util.toast("Ocorreu erro ao atualizar este material");
      }).success(function (data) {
        util.toast("Material atualizado com sucesso");
        $location.path("/stuffs");
      })
    } else {
      stuffServiceAPI.create(stuff).error(function (response) {
        util.toast("Ocorreu erro ao criar este material");
      }).success(function (data) {
        util.toast("Material criado com sucesso");
        $location.path("/stuffs");
      });
    }
	}
});