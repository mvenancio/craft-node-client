angular.module("craft-app").service("util", function () {
	this.formatDate = function (date, currentPattern, toPattern)  {
		if (date) {
          return moment(date, currentPattern).format(toPattern)
        }
        return date;
    }

    this.round = function(value) {
        if (value) 
    	   return parseFloat(value.toFixed(2));
        return value;
    },

    this.toast = function(message) {
		Materialize.toast(message, 5000);
    }
})