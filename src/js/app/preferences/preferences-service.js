angular.module("craft-app").service("preferencesAPI", function ($http) {
	this.getPreferences = function () {
		return $http.get("/api/preferences");
	};

	this.update = function (preferences) {
		return $http.put("/api/preferences", preferences);
	}
	this.create = function (preferences) {
		return $http.post("/api/preferences", preferences);
	}
	this.upload = function (file) {
		var fd = new FormData();
        fd.append('sampleFile', file);
		return $http.post("/api/upload", fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               });
	}
});