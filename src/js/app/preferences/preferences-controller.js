angular.module("craft-app").controller('PreferencesController', 
  function ($scope, $location,$timeout, preferencesAPI, signatureService, util) {
  
  $scope.load = function () {
    preferencesAPI.getPreferences().success( function (response) {
      $scope.preferences = response;
      $timeout(reloadImg, 500);
    });
    signatureService.get().success(function (response) {
      $scope.signature = response;
    });
  };

  $scope.filesChanged = function(el) {
    $scope.files = el.files;
    $scope.$apply();
  };

  $scope.uploadFile = function() {
    var file = $scope.myFile;

    preferencesAPI.upload(file).success(function(data) {
      console.log(data);
      $scope.preferences.image = data;
      $timeout(reloadImg, 500);
      util.toast("Upload OK");
    }).error(function() {
      util.toast("Upload ERRO");
    });
  }
  var reloadImg = function () {
    console.log($scope.preferences.image);
    if (!$scope.preferences.image) {
      return;
    }
    var 
    path = $scope.preferences.image.path,
    sufix = path.substring(path.length-4, path.length);
    path = path.substring(path.indexOf("user"), path.length);

    path = path.replace(sufix,"-min"+sufix);
    $scope.preferences.logo = path;
  }

  $scope.getEndDate = function() {
    if ($scope.signature) {
      return util.formatDate($scope.signature.endDate, "YYYY-MM-DD hh:mm", "DD/MM/YYYY  hh:mm");
    }
  }

  $scope.getSignatureStatus = function() {
    if ($scope.signature) {
      switch ($scope.signature.status) {
        case 'PENDING':
          return "Pendente"
        case 'REQUESTED':
          return "Solicitada"
        case 'REFUSED':
          return "Recusada"
        case 'ACTIVE':
          return "Ativo"
        case 'INACTIVE':
          return "Inativo"
        default: 
          break;
      }
    }
  }

  $scope.cancelSignature = function() {
    $scope.loading = true;
    signatureService.delete().success(function (response) {
      $scope.loading = false;
      util.toast("Assinatura cancelada com Sucesso. Você conseguirá utilizar o sistema até a data final");
      $scope.load();
    }).error(function () {
      $scope.loading = false;
      util.toast("Ocorreu um erro ao solicitar o cancelamento da assinatura. Tente novamente mais tarde");
    });
  }

  $scope.createSignature = function() {
    $scope.loading = true;
    signatureService.create().success(function (response) {
      if (response.redirectUrl) {
        window.location = response.redirectUrl;
      }
      $scope.loading = false;
      util.toast("Assinatura solicitada com Sucesso.");
    }).error(function () {
      $scope.loading = false;
      util.toast("Não foi possível criar a assinatura. Tente novamente mais tarde!");
    });
  }

  $scope.shouldCancel = function () {
    if ($scope.signature) {
      switch ($scope.signature.status) {
        case 'PENDING':
          return false;
        case 'INACTIVE':
          return false;
        case 'REFUSED':
          return false;      
        default: 
          return true;
      }
    }
    return false;
  }

  $scope.shouldCreate = function () {
    return !$scope.shouldCancel();
  }

  $scope.save = function (preferences) {
    preferences.valueByHour = preferences.profit / preferences.hours;
    if (preferences.id) {
      preferencesAPI.update(preferences)
      .error(function (response) {
        util.toast("Erro ao salvar. Tente novamente mais tarde!");
      }).success(function (data) {
        util.toast("Preferencias salvas com sucesso");
        $location.path("/dashboard")
      })
    } else {
      $preferencesAPI.create(preferences).error(function (response) {
        util.toast("Erro ao salvar. Tente novamente mais tarde!</span>");
      }).success(function (data) {
        util.toast("Preferencias salvas com sucesso!");
        $location.path("/dashboard")
      });
    }
  };
  $scope.load();
});