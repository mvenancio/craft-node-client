angular.module("craft-app").service("paymentAPI",function ($http) {
	this.pay = function (payment) {
		return $http.post("/api/payments/pay",payment);
	}

	this.cancel = function (payment) {
		return $http.post("/api/payments/cancel",payment);
	}

	this.getExpired = function (payment) {
		return $http.get("/api/payments/expired");
	}

})