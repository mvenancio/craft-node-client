angular.module("craft-app").service("customerServiceAPI", function($http) {
	this.get = function () {
		return $http.get("/api/customers");
	};
	this.load = function (id) {
		return $http.get("/api/customers/"+id);
	};
	this.create = function (customer) {
		return $http.post("/api/customers", customer);
	};
	this.update = function (customer) {
		return $http.put("/api/customers", customer);
	};
})