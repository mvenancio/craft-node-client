angular.module('craft-app').controller('CustomerFormController',
   function ($scope, $location, $routeParams, util, customerServiceAPI) {
    if ($routeParams.id) {
      customerServiceAPI.load($routeParams.id).success( function (response){
        $scope.customer = response;
        $scope.customer.birthdate = util.formatDate($scope.customer.birth, "YYYY-MM-DD", "DD/MM/YYYY");
      })
    }
    $scope.save = function (customer) {
      customer.birth = util.formatDate(customer.birthdate, "DD/MM/YYYY","YYYY-MM-DD");
      if (customer.id) {
        customerServiceAPI.update(customer).error(function (response) {
          util.toast("Erro ao atualizar o cliente");
        }).success(function (data) {
          util.toast("Cliente atualizado com sucesso");
          $location.path("/customer")
        })
      } else {
        customerServiceAPI.create(customer).error(function (response) {
          util.toast("Erro ao criar o cliente");
        }).success(function (data) {
          util.toast("Cliente criado com sucesso");
          $location.path("/customer")
        });
      }
    };
});

