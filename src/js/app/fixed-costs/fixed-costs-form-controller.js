angular.module('craft-app').controller('FixedCostsFormController', 
  function ($scope, $location, $routeParams, fixedCostsAPI,  util) {
  if ($routeParams.id) {
    fixedCostsAPI.load($routeParams.id).success( function(response) {
      $scope.fixedCost = response
    });  
  }
  $scope.save = function (fixedCost) {
    if (fixedCost.id) {
      fixedCostsAPI.update(fixedCost).error(function (response) {
        util.toast("Erro ao atualizar o custo fixo");
      }).success(function (data) {
        util.toast("Custo fixo atualizado com sucesso");
        $location.path("/fixed-costs")
      });
    } else {
      fixedCostsAPI.create(fixedCost).error(function (response) {
        util.toast("Erro ao criar o custo fixo");
      }).success(function (data) {
        util.toast("Custo fixo criado com sucesso");
        $location.path("/fixed-costs")
      });
    }
  };
});
