angular.module('craft-app').service("fixedCostsAPI", function ($http) {
	this.get = function() {
		return $http.get("/api/fixed-costs")
	}

	this.load = function(id) {
		return $http.get("/api/fixed-costs/"+id);
	}

	this.getTotal = function() {
		return $http.get("/api/fixed-costs/total");
	}

	this.update = function(fixedCost) {
		return $http.put("/api/fixed-costs", fixedCost)
	}

	this.create = function(fixedCost) {
		return $http.post("/api/fixed-costs", fixedCost)
	}

})