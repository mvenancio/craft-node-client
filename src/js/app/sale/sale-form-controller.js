angular.module("craft-app").controller('SaleFormController', 
  function ($scope, $routeParams,$location, util, saleAPI, customers, products, sellers) {
  $scope.customers = customers.data.items;
  $scope.products = products.data.items;
  $scope.sellers = sellers.data.items;
  $scope.sale = {
    discountValue: 0,
    discountPercentage: 0,
    installments: 1,
    status: 'ESTIMATE'
  };
  $scope.selectedProducts =[];
  var shouldCalculate = true;

  if ($routeParams.id) {
    saleAPI.load($routeParams.id).success(function (response) {
      shouldCalculate = false;
      $scope.sale = response;
      $scope.sale.formatedDeliveryDate = util.formatDate(response.deliveryDate, "YYYY-MM-DD", "DD/MM/YYYY") ;
      $scope.sale.formatedFirstDueDate = util.formatDate(response.firstDueDate, "YYYY-MM-DD", "DD/MM/YYYY") ;
      if (response.items) {
        response.items.forEach(function (item) {
          var product = _.find($scope.products, function(product) {
            return product.id == item.productId;
          });
          product.quantity = item.amount;
          product.observation = item.description;
          $scope.selectedProducts.push(product);
        });
      }
      shouldCalculate = true;
    });
  }

  $scope.getCurrentStatus = function () {
    return saleAPI.getCurrentStatus($scope.sale);
  }

  var getItems = function () {
    var items = [];
    $scope.selectedProducts.forEach (function (product) {
      var item = {
        productId: product.id,
        amount: product.quantity,
        totalValue: util.round(product.price * product.quantity),
        description: product.observation
      }
      items.push(item);
    });
    return items;
  }

  $scope.calculatePrice = function () {
    if (!shouldCalculate) {
      return;
    }
    var totalPrice = 0;
    var totalCost = 0;
    var totalWorkCost = 0;
    var comission = 0;
    if ($scope.selectedProducts) {
      $scope.selectedProducts.forEach(function (product) {
        quantity = product.quantity && product.quantity != "" ? parseInt(product.quantity) : 0;
        totalPrice += util.round(product.price * quantity);
        totalCost += util.round(product.totalCost * quantity);
        totalWorkCost += util.round(product.workCost * quantity);
      });
    }
    
    if ($scope.sale.seller) {
      comission = util.round ((totalPrice * $scope.sale.seller.percentage)/100);
    }
    $scope.sale.totalCostsValue = totalCost;
    $scope.sale.workValue = totalWorkCost;
    $scope.sale.rateValue = comission;
    if ($scope.sale.discountValue) {
      totalPrice -= $scope.sale.discountValue;
    }
    $scope.sale.totalValue = totalPrice;
    $scope.sale.profitValue = $scope.sale.totalValue - ($scope.sale.totalCostsValue + $scope.sale.rateValue );
  }

  var getSelectedProducts = function (items) {
    var products = [];
    if (items) {
      items.forEach(function (item) {
        for(var i = 0; i < $scope.products.length; i++) {
          var product = $scope.products[i];
          if (product.id == item.productId) {
            product.quantity = item.amount;
            product.quantity = item.amount;
            products.push(product);
            break;
          }
        }
      })
    }
    return products;
  }

   $scope.checkUncheck = function (item) {        
      if (item.added) {
        selectedProduct(item);
      } else {
        $scope.remove(item);
      }
      $scope.calculatePrice(); 
  }

  $scope.selectedCustomer = function (customer) {
    $scope.sale.customer = customer;
  };
  $scope.removeCustomer = function() {
    delete $scope.sale.customer;
  }

  $scope.selectedSeller = function (seller) {
      $scope.sale.seller = seller;
      $scope.calculatePrice();
  };

  function selectedProduct (item )  {
    $scope.selectedProducts.push(item);
  }

  $scope.calculateDiscountValue = function () {
    if (!shouldCalculate) {
      return;
    }
    var discountValue = 0;
    if ($scope.sale.totalValue > 0 && $scope.sale.discountPercentage > 0) {
      discountValue = util.round(($scope.sale.totalValue * $scope.sale.discountPercentage) / 100);
    } 
    $scope.sale.discountValue = discountValue;
  }

  $scope.calculateDiscountPercentage = function () {
    if (!shouldCalculate) {
      return;
    }
    var discountPercentage = 0;
    if ($scope.sale.totalValue > 0 && $scope.sale.discountValue > 0) {
      discountPercentage = util.round(($scope.sale.discountValue / $scope.sale.totalValue ) * 100);
    }
    $scope.sale.discountPercentage = util.round(discountPercentage);
  }

  $scope.remove = function (item) {
    delete item.added;
    var index = $scope.selectedProducts.indexOf(item);
    $scope.selectedProducts.splice(index, 1);
  }

  $scope.save = function (sale) {
    if (sale.seller) {
      sale.sellerId = sale.seller.id;
    }
    sale.customerId = sale.customer.id;
    sale.items = getItems();
    sale.deliveryDate = util.formatDate(sale.formatedDeliveryDate, "DD/MM/YYYY", "YYYY-MM-DD") ;
    sale.firstDueDate = util.formatDate(sale.formatedFirstDueDate, "DD/MM/YYYY", "YYYY-MM-DD") ;
    sale.partyDate = util.formatDate(sale.partyDate, "DD/MM/YYYY", "YYYY-MM-DD") ;
    if (sale.id) {
      saleAPI.update(sale).error(function (response) {
        util.toast("Ocorreu erro ao atualizar o pedido");
      }).success(function (data) {
        util.toast("Pedido atualizado com sucesso");
        $location.path("/sale")
      })
    } else {
      saleAPI.create(sale).error(function (response) {
        util.toast("Ocorreu erro ao criar o pedido");
      }).success(function (data) {
        util.toast("Pedido criado com sucesso");
        $location.path("/sale")
      });
    }
  };
});