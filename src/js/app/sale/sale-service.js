angular.module("craft-app").service("saleAPI", function ($http) {
	this.get = function () {
		return $http.get("/api/sales");
	}

  this.load = function (id) {
    return $http.get("/api/sales/"+id);
  }

  this.create = function (sale) {
    return $http.post("/api/sales",sale);
  }

  this.update = function (sale) {
    return $http.put("/api/sales",sale);
  }

	this.getEstimates = function () {
		return $http.get("/api/sales/estimates");
	}

  this.getPaymentsFor = function(sale) {
    return $http.get("/api/sales/"+sale.id+"/payments");
  }

	this.getFinished = function  () {
		return $http.get("/api/sales/finisheds/summary");
	}

	this.approve = function (sale) {
		return $http.post("/api/sales/approve", sale);
	}
	
	this.cancel = function (sale) {
		return $http.post("/api/sales/cancel", sale);
	}

	this.getCurrentStatus = function (sale) {
        switch (sale.status) {
          case "CANCELLED":
            return "CANCELADO";
          case "APPROVED":
            return "APROVADO";
          case "FINISHED":
            return "FINALIZADO";
          default:
            return "ORÇAMENTO"
        }
      }

    this.getCurrentPaymentStatus = function (payment) {
        switch (payment.status) {
          case "PAID":
            return "PAGO";
          case "CANCELLED":
            return "CANCELADO";
          default:
            return "PENDENTE"
        }
      }
})