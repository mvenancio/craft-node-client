angular.module("craft-app").controller('SaleController', function ($scope, sales, paymentAPI, saleAPI, util) {
  $scope.sales = sales.data.items

  $scope.loadPayments = function (sale) {
    $scope.salePayment = sale;
    saleAPI.getPaymentsFor(sale).success( function (response) {
      $scope.payments = response.items;
      $scope.payment = true;
      delete $scope.sales;
    })
  };

  $scope.closePayments = function () {
    delete $scope.payment;
    delete $scope.salePayment;  
    delete $scope.payments;
    reload();
  };

  $scope.approve = function (sale) {
    saleAPI.approve(sale).error(function (response) {
      util.toast("Ocorreu erro ao aprovar a venda");
    }).success(function (data) {
      util.toast("Venda aprovada com sucesso");
      reload();
    });
  };

  $scope.cancelSale = function (sale) {
    saleAPI.cancel(sale).error(function (response) {
      util.toast("Ocorreu erro ao cancelar a venda");
    }).success(function (data) {
      util.toast("Venda cancelada com sucesso");
      reload();
    });
  };

  $scope.pay = function (payment) {
    paymentAPI.pay(payment).error(function (response) {
      util.toast("Ocorreu erro ao confirmar o pagamento");
    }).success(function (data) {
      util.toast("Pagamento confirmado com sucesso");
      $scope.loadPayments($scope.salePayment);        
    });
  };

  $scope.cancel = function (payment) {
    paymentAPI.cancel(payment).error(function (response) {
       util.toast("Ocorreu erro ao cancelar o pagamento");
    }).success(function (data) {
      util.toast("Pagamento cancelado com sucesso");
      $scope.loadPayments($scope.salePayment);
    });
  };

  $scope.translate = function (sale) {
    return saleAPI.getCurrentStatus(sale);
  }

  $scope.getCurrentPaymentStatus = function (payment) {
    return saleAPI.getCurrentPaymentStatus(payment);
  }
 
  function reload() {
    saleAPI.get().success(function(response) {
      $scope.sales = response.items;  
    })
  }

});
