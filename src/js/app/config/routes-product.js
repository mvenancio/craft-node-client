angular.module("craft-app").config(function ($routeProvider) {
	$routeProvider.when("/product", {
		templateUrl: "templates/product/list.html",
		controller: "ProductController",
		resolve: {
			products: function (productServiceAPI) {
				return productServiceAPI.get();
			}
		}
	});
	$routeProvider.when("/product/new", {
		templateUrl: "templates/product/form.html",
		controller: "ProductFormController",
		resolve: {
			stuffs: function (stuffServiceAPI) {
				return stuffServiceAPI.get();
			},
			depreciationCosts: function (depreciationCostsApi) {
				return depreciationCostsApi.get();
			},
			totalFixedCost: function (fixedCostsAPI) {
				return fixedCostsAPI.getTotal();
			},
			preferences: function (preferencesAPI) {
				return preferencesAPI.getPreferences()
			}
		}
	});
	$routeProvider.when("/product/edit/:id", {
		templateUrl: "templates/product/form.html",
		controller: "ProductFormController",
		resolve: {
			stuffs: function (stuffServiceAPI) {
				return stuffServiceAPI.get();
			},
			depreciationCosts: function (depreciationCostsApi) {
				return depreciationCostsApi.get();
			},
			totalFixedCost: function (fixedCostsAPI) {
				return fixedCostsAPI.getTotal();
			},
			preferences: function (preferencesAPI) {
				return preferencesAPI.getPreferences()
			}
		}
	});
});