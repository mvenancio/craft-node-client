angular.module("craft-app").config(function ($routeProvider) {

	$routeProvider.when("/seller",{
		templateUrl: "templates/seller/list.html",
		controller: "SellerController",
		resolve: {
			sellers: function (sellerServiceAPI) {
				return sellerServiceAPI.get();
			}
		}
	});
	$routeProvider.when("/seller/new",{
		templateUrl: "templates/seller/form.html",
		controller: "SellerFormController"
	});
	$routeProvider.when("/seller/edit/:id",{
		templateUrl: "templates/seller/form.html",
		controller: "SellerFormController"
	});
	
});