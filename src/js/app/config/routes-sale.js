angular.module("craft-app").config(function ($routeProvider) {

	$routeProvider.when("/sale",{
		templateUrl: "templates/sale/list.html",
		controller: "SaleController",
		resolve: {
			sales: function (saleAPI) {
				return saleAPI.get();
			}
		}
	});
	$routeProvider.when("/sale/new",{
		templateUrl: "templates/sale/form.html",
		controller: "SaleFormController",
		resolve: {
			products: function (productServiceAPI) {
				return productServiceAPI.get();
			},
			sellers: function(sellerServiceAPI) {
				return sellerServiceAPI.get();				
			},
			customers: function(customerServiceAPI) {
				return customerServiceAPI.get();				
			}
		}
	});
	$routeProvider.when("/sale/edit/:id",{
		templateUrl: "templates/sale/form.html",
		controller: "SaleFormController",
		resolve: {
			products: function (productServiceAPI) {
				return productServiceAPI.get();
			},
			sellers: function(sellerServiceAPI) {
				return sellerServiceAPI.get();				
			},
			customers: function(customerServiceAPI) {
				return customerServiceAPI.get();				
			}
		}
	});
	
});