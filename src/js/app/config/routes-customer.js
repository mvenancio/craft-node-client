angular.module("craft-app").config(function ($routeProvider) {
	$routeProvider.when("/customer", {
		templateUrl: "templates/customer/list.html",
		controller: "CustomerController",
		resolve: {
			customers: function (customerServiceAPI) {
				return customerServiceAPI.get();
			}
		}
	});
	$routeProvider.when("/customer/new",{
		templateUrl: "templates/customer/form.html",
		controller: "CustomerFormController"
		
	});
	$routeProvider.when("/customer/edit/:id",{
		templateUrl: "templates/customer/form.html",
		controller: "CustomerFormController"
		
	})
});