angular.module("craft-app").config( function ($routeProvider) {
	$routeProvider.when("/stuffs/new", {
		templateUrl: "templates/stuff/form.html",
		controller: "StuffFormController"
	});
	$routeProvider.when("/stuffs/edit/:id", {
		templateUrl: "templates/stuff/form.html",
		controller: "StuffFormController"
	});
	$routeProvider.when("/stuffs", {
		templateUrl: "templates/stuff/list.html",
		controller: "StuffController",
		resolve: {
			stuffs: function (stuffServiceAPI) {
				return stuffServiceAPI.get();
			}
		}
	});
});