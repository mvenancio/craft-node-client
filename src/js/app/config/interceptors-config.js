angular.module("craft-app").config(function ($httpProvider){
	$httpProvider.interceptors.push("timestampInterceptor");
	$httpProvider.interceptors.push("securityInterceptor");
})