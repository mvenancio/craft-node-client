angular.module("craft-app").config(function($routeProvider) {
	$routeProvider.otherwise({
		redirectTo:"/dashboard"
	});
	$routeProvider.when("/expired", {
		templateUrl:"templates/expired.html",
	});
	$routeProvider.when("/dashboard",{
		templateUrl:"templates/home.html",
		controller: "HomeController",
		resolve: {
			payments: function (paymentAPI) {
				return paymentAPI.getExpired();
			},
			sales: function(saleAPI) {
				return saleAPI.getEstimates();
			},
			preferences: function (preferencesAPI) {
				return preferencesAPI.getPreferences()
			},
			finished: function (saleAPI) {
				return saleAPI.getFinished();
			}
		}
	});
	$routeProvider.when("/preferences",{
		templateUrl:"templates/preferences.html",
		controller: "PreferencesController"
	});
	$routeProvider.when("/fixed-costs",{
		templateUrl:"templates/fixed-cost/list.html",
		controller: "FixedCostsController",
		resolve: {
			fixedCosts: function (fixedCostsAPI) {
				return fixedCostsAPI.get();
			}
		}
	});
	$routeProvider.when("/fixed-costs/new",{
		templateUrl:"templates/fixed-cost/form.html",
		controller: "FixedCostsFormController"
	});
	$routeProvider.when("/fixed-costs/edit/:id",{	
		templateUrl:"templates/fixed-cost/form.html",
		controller: "FixedCostsFormController"
	});
	$routeProvider.when("/depreciation-costs",{
		templateUrl:"templates/depreciation-cost/list.html",
		controller: "DepreciationCostsController",
		resolve: {
			depreciationCosts: function (depreciationCostsApi) {
				return depreciationCostsApi.get();
			}
		}
	});
	$routeProvider.when("/depreciation-costs/new",{
		templateUrl:"templates/depreciation-cost/form.html",
		controller: "DepreciationCostsFormController"
	});
	$routeProvider.when("/depreciation-costs/edit/:id",{
		templateUrl:"templates/depreciation-cost/form.html",
		controller: "DepreciationCostsFormController"
	});
	
});