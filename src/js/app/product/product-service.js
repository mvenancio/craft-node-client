angular.module("craft-app").service("productServiceAPI", function($http) {
	this.get = function() {
		return $http.get("/api/products");
	}
	this.load = function(id) {
		return $http.get("/api/products/"+id);
	}
	this.create = function(product) {
		return $http.post("/api/products",product);
	}
	this.update = function(product) {
		return $http.put("/api/products",product);
	}
	this.upload = function(file) {
		var fd = new FormData();
        fd.append('file', file);
		return $http.post("/api/products/upload", fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               });
	}
})