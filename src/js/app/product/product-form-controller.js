angular.module('craft-app').controller('ProductFormController',
    function ($scope, $location, $routeParams, $rootScope, util, stuffs, depreciationCosts, totalFixedCost,
              preferences, productServiceAPI) {
      $scope.preferences = preferences.data;
      $scope.selectedStuffs = [];
      $scope.selectedDepreciationCosts = [];
      $scope.stuffs = stuffs.data.items;
      $scope.depreciationCosts = depreciationCosts.data.items;
      $scope.margin = util.round(preferences.data.margin);

      $scope.totalCosts = parseFloat(totalFixedCost.data);// + parseFloat(responses[1].data);
      $scope.costsBySeconds = $scope.totalCosts/($scope.preferences.hours*(3600));

      if ($routeParams.id) {
        productServiceAPI.load($routeParams.id).success(function(response) {
          $scope.product = response;
          $scope.product.time = getTime($scope.product.productionTime);
          fillSelectedStuffs($scope.product.items);
          fillSelectedDepreciation($scope.product.costs);
          $scope.calculateSuggestedPrice();
        });
      }

      $scope.calculateMargin = function() {
        var netPrice = round($scope.product.price - $scope.product.totalCost);
        var costs = ($scope.product.totalCost);
        $scope.product.percentage = round((netPrice / costs)*100);
      }

      $scope.applyPrice = function() {
        if ($scope.apply) {
          if ($scope.precoSugerido)
            $scope.product.price = round($scope.precoSugerido);
            $scope.product.percentage = $scope.margin;
        }
      }
      
      $scope.calculateSuggestedPrice = function () {
        var precoSugerido = 0;
        var margin = $scope.preferences.margin;
        if($scope.product) {
          $scope.calculateCosts();
          precoSugerido += $scope.product.compositionCost;
        }
        if (precoSugerido > 0 ) {
          precoSugerido = round(precoSugerido+round((precoSugerido * margin)/100));
          precoSugerido += $scope.product.fixedCost;
          precoSugerido += $scope.product.depreciationCost;
          precoSugerido += $scope.product.workCost;
        }
        if (isNaN(precoSugerido)) {
          precoSugerido = 0;
        }
        $scope.precoSugerido = precoSugerido;
        if ($scope.apply) {
          if ($scope.precoSugerido)
            $scope.product.price = $scope.precoSugerido;
        }
      };

      $scope.calculatePrice = function () {
        var precoSugerido = 0,
            margin = 0;
        if (!$scope.product) {
          $scope.calculateCosts();
          precoSugerido += $scope.product.compositionCost;
          precoSugerido += $scope.product.fixedCost;
        }
        if (precoSugerido > 0 ) {
          if ($scope.product.percentage)  {
            margin = $scope.product.percentage;
          }
          precoSugerido = round(precoSugerido+round((precoSugerido * margin)/100));
          precoSugerido += $scope.product.workCost;
        }
        if (margin != $scope.margin) {
          delete $scope.apply;
        }
        if (isNaN(precoSugerido) || precoSugerido <= 0) {
          return;
        }
        $scope.product.price = round(precoSugerido);
        
      };

      $scope.calculateCosts = function () {
        var tempo = calculateTime($scope.product);
        var composicao = getCompositionToCalculate();
        var totalCost = 0;
        
        if (composicao) {
          var compositionCost = 0; 
          composicao.forEach( function (item) {
            compositionCost += item.unitPrice * item.quantity;
          });
          $scope.product.compositionCost = round(compositionCost);
          totalCost += round(compositionCost);
        }

        $scope.product.depreciationCost = 0;
        if ($scope.selectedDepreciationCosts && tempo) {
          var depreciationCost = 0; 
          $scope.selectedDepreciationCosts.forEach(function (depreciation) {
            var valorMes = (depreciation.value / depreciation.durability);
            var valorDia = (valorMes / 30);
            var valorHora = (valorDia / 24);
            var valorMinuto = (valorHora / 60);
            var valorSegundo = (valorMinuto / 60);
            depreciationCost += (valorSegundo * tempo);
            depreciation.cost = (valorSegundo * tempo);
          });
          $scope.product.depreciationCost = round(depreciationCost);
          totalCost += round(depreciationCost);
        }

        if (tempo) {
          $scope.product.fixedCost = round(tempo * $scope.costsBySeconds);
          totalCost += $scope.product.fixedCost;
          $scope.product.workCost = round(tempo * getWorkValueInSeconds());
          totalCost += $scope.product.workCost;
        }
        $scope.product.totalCost = totalCost;
      };
      
      $scope.checkUncheck = function (item) {        
        if (item.added) {
          selectedStuff(item);
        } else {
          $scope.remove(item);
        }
        $scope.calculateCosts(); 
        $scope.calculateSuggestedPrice(); 
      }

      $scope.checkUncheckDepreciation = function (item) {        
        if (item.checked) {
          selectedDepreciationCost(item);
        } else {
          $scope.removeDepreciationCost(item);
        }
        $scope.calculateCosts(); 
        $scope.calculateSuggestedPrice(); 
      }
      
      $scope.calculateQuantity = function (stuff) {
        if (stuff.isYield) {
          stuff.quantity = 1 / stuff.filledQuantity;
        } else {
          stuff.quantity = stuff.filledQuantity;;
        }
      }
      
      $scope.save = function (product) {
        product.productionTime = moment.duration( product.time ).asSeconds();
        product.composition = getComposition();
        product.costComposition = getCostComposition();
        console.log(product);
        if (product.id) {
          productServiceAPI.update(product).error(function (response) {
            util.toast("Erro ao atualizar o produto");
          }).success(function (data) {
            util.toast("Produto atualizado");
            $location.path("/product")
          })
        } else {
          productServiceAPI.create(product).error(function (response) {
            util.toast("Erro ao criar o produto");
          }).success(function (data) {
            util.toast("Produto criado!");
            $location.path("/product")
          });
        }
      };

      function calculateTime (product) {
        if (product.time) {
          return moment.duration( product.time ).asSeconds();
        }
      };

      function getWorkValueInSeconds () {
        return parseFloat($scope.preferences.valueByHour/3600);
      }

      function round (price) {
        return util.round(price);
      }

      function selectedStuff (item )  {
        if (item.isYield == undefined) {
          item.isYield = false;
        } 
        $scope.selectedStuffs.push(item);
      }

      function selectedDepreciationCost (item )  {
        $scope.selectedDepreciationCosts.push(item);
      }
      
      $scope.remove = function (item) {
        delete item.added;
        var index = $scope.selectedStuffs.indexOf(item);
        $scope.selectedStuffs.splice(index, 1);
      }

      $scope.removeDepreciationCost = function (item) {
        delete item.checked;
        var index = $scope.selectedDepreciationCosts.indexOf(item);
        $scope.selectedDepreciationCosts.splice(index, 1);
      }

      function fillSelectedStuffs (items) {
        if($scope.stuffs) {
          $scope.stuffs.forEach(function (stuff) {
            if (items) {
              for(var x = 0; x < items.length; x++) {
                var item = items[x];
                if (item.stuff.id == stuff.id) {
                  var newStuff = stuff;
                  newStuff.added = true;
                  newStuff.quantity = item.quantity;
                  newStuff.isYield = newStuff.quantity < 1;
                  newStuff.filledQuantity = newStuff.quantity;
                  if (newStuff.isYield){
                    newStuff.filledQuantity = 1 / newStuff.filledQuantity;
                  }
                  $scope.selectedStuffs.push(newStuff);
                }
              }
              $scope.calculatePrice();
            }
          });
        }
      }

      function fillSelectedDepreciation (items) {
        if($scope.depreciationCosts) {
          $scope.depreciationCosts.forEach(function (depreciation) {
            if (items) {
              for(var x = 0; x < items.length; x++) {
                var item = items[x];
                if (item.depreciation.id == depreciation.id) {
                  var newDepreciation = depreciation;
                  newDepreciation.checked = true;
                  newDepreciation.cost = item.value;
                  $scope.selectedDepreciationCosts.push(newDepreciation);
                }
              }
              $scope.calculatePrice();
            }
          });
        }
      }
    
      function getComposition () {
        var composition = {};
        $scope.selectedStuffs.forEach (function ( stuff ) {
          if (stuff.quantity) {
            composition[stuff.id.toString()] = stuff.quantity;
          }
        });
        return composition;
      };

      function getCostComposition () {
        var composition = {};
        $scope.selectedDepreciationCosts.forEach (function ( depreciation ) {
          composition[depreciation.id.toString()] = depreciation.cost;
        });
        return composition;
      };

      function getCompositionToCalculate () {
        if($scope.selectedStuffs){
          var composition = [];
          $scope.selectedStuffs.forEach (function ( stuff ) {
            if (stuff.quantity) {
              composition.push(stuff);
            }
          });
          return composition;
        }
      };
      function getTime (millis) {
        return moment.duration(millis, "seconds").format("hh:mm:ss", { trim: true });
      };
});
