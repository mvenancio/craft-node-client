angular.module('craft-app').controller('UsersController', function ($scope, userService) {
  $scope.loading = false;
  $scope.save = function (user) {
    $scope.loading = true;
    userService.create(user).success(function (data) {
      $scope.loading = false
      $scope.messageClass = "green-text"
      $scope.message = "Parabéns! Seu pedido foi solicitado com sucesso. Faça seu login e começe já a usar nossa plataforma"
      console.log(data);
      window.setTimeout('window.location = "/system"', 1500);
      if (data.redirectUrl) {
        window.location = data.redirectUrl;
      }
    }).error(function (data, status) {
      $scope.loading = false
      if (status == 422) {
        userService.rememberMe(user.email).success( function (response) {
          $scope.messageClass = "green-text"
          $scope.message = "O email para confirmação de seu cadastro foi reenviado"
          $scope.reset();
          
        }).error(function (response) {
          $scope.messageClass = "red-text"
          $scope.message = "Erro ao reenviar o email de confirmação. Tente novamente mais tarde."
        });
      } else {
        $scope.messageClass = "red-text"
        $scope.message = "Erro ao criar o usuário. Tente novamente mais tarde."
      }
    });
  };

  $scope.validateRegister = function () {
    var valid = false;
    if ($scope.user && $scope.user.email && $scope.user.password == $scope.user.confirmPassword) {
      valid = true;
    }
    $scope.isUserValid = valid;
  }
  $scope.isUserValid = false;
});
