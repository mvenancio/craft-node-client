angular.module("craft-app").service("userService", function($http) {
	this.create = function(user) {
		return $http.post("/api/users", user)
	}
	this.rememberMe = function(email) {
		return $http.get("/api/users/remember?email=" + email);
	}
});