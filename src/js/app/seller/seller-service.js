angular.module("craft-app").service("sellerServiceAPI", function ($http) {
	this.get = function () {
		return $http.get("/api/sellers");
	}
	this.load = function (id) {
		return $http.get("/api/sellers/" + id);
	}
	this.update = function (seller) {
		return $http.put("/api/sellers", seller);
	}
	this.create = function (seller) {
		return $http.post("/api/sellers", seller);
	}
});