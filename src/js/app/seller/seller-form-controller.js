angular.module("craft-app").controller("SellerFormController", 
	function ($scope, $location, $routeParams, util, sellerServiceAPI) {
	if($routeParams.id) {
		sellerServiceAPI.load($routeParams.id).success(function (response){
			$scope.seller = response;
		})
	}

	$scope.save = function (seller) {
		if (seller.id) {
			sellerServiceAPI.update(seller).success(function() {
				util.toast("Canal de venda atualizado com sucesso");
				$location.path("/seller");
			}).error(function () {
				util.toast("Erro ao atualizar o canal de venda");
			});
		} else {
			sellerServiceAPI.create(seller).success(function () {
				util.toast("Canal de venda criado com sucesso");
				$location.path("/seller");
			}).error(function () {
				util.toast("Erro ao criar o canal de venda");
			});
		}
	}

});