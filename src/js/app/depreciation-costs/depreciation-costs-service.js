angular.module('craft-app').service("depreciationCostsApi", function($http) {
	this.get = function() {
		return $http.get("/api/depreciation-costs");
	}
	this.getTotal = function() {
		return $http.get("/api/depreciation-costs/total");
	}
	this.load = function(id) {
		return $http.get("/api/depreciation-costs/"+id);
	}
	this.update = function(depreciationCost) {
		return $http.put("/api/depreciation-costs", depreciationCost);
	}
	this.create = function(depreciationCost) {
		return $http.post("/api/depreciation-costs", depreciationCost);
	}
});