angular.module('craft-app').controller('DepreciationCostsFormController', 
	function ($scope, $location, $routeParams,util, depreciationCostsApi) {
  if ($routeParams.id) {
    depreciationCostsApi.load($routeParams.id).success( function(response) {
      $scope.depreciationCost = response;
    });  
  }

  $scope.save = function (depreciationCost) {
    if (depreciationCost.id) {
      depreciationCostsApi.update(depreciationCost).error(function (response) {
        util.toast("Ocorreu erro ao atualizar o custo de depreciação");
      }).success(function (data) {
      	util.toast("Custo de depreciação atualizado com sucesso");
      	$location.path("/depreciation-costs");
      })
    } else {
      depreciationCostsApi.create(depreciationCost).error(function (response) {
        util.toast("Ocorreu erro ao criar o custo de depreciação");
      }).success(function (data) {
      	util.toast("Custo de depreciação criado com sucesso");
      	$location.path("/depreciation-costs");
      });
    }
  };
});
