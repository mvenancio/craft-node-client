angular.module('craft-app', ["ngRoute", "chart.js"]).controller('HomeController', 
  function ($scope, $q, $location, payments, sales, preferences, finished, util, paymentAPI, saleAPI) {
  $scope.payments = payments.data.items;
  $scope.sales = sales.data.items;
  $scope.preferences = preferences.data;
  if (!$scope.preferences.profit) {
    $location.path("/preferences");
  }
  // var summary = finished.data;
  // var labels = [];
  // var series = ["Lucro", "M.O.", "Custos", "Taxas"];
  // var profitData = []
  // var workData = [];
  // var costsData = [];
  // var totalValueData = [];
  // var rateData = [];
  // for (var attr in summary ) {
  //   var sales = summary[attr];
  //   labels.push(util.formatDate(sales.monthYear, "YYYY-MM", "MM/YYYY"));
  //   profitData.push(util.round(sales.profitValue));
  //   workData.push(util.round(sales.workValue));
  //   costsData.push(util.round(sales.totalCostsValue));
  //   rateData.push(util.round(sales.rateValue));
  // }

  // $scope.labels = labels;
  // $scope.series = series;
  // $scope.data = [profitData, workData, costsData, rateData];
  
  // $scope.onClick = function (points, evt) {
  //   console.log(points, evt);
  // };
  var reload = function() {
    $q.all([
      saleAPI.getEstimates(),
      paymentAPI.getExpired()
    ]).then( function(results) {
      $scope.payments = results[1].data.items;
      $scope.sales = results[0].data.items;
    });
  }

  var doPost = function (post, body,  successMessage, errorMessage) {
    post(body).error(function (response) {
      util.toast(errorMessage);
    }).success(function (data) {
      util.toast(successMessage);      
      reload();
    });
  };
  
  $scope.approve = function (sale) {
    doPost(saleAPI.approve, sale, "Venda aprovada com sucesso", "Ocorreu erro ao aprovar a venda");
  };

  $scope.cancelSale = function (sale) {
    doPost(saleAPI.cancel, sale, "Venda cancelada com sucesso", "Ocorreu erro ao cancelar a venda");
  };

  $scope.pay = function (payment) {
    doPost(paymentAPI.pay, payment, "Pagamento efetuado com sucesso", "Ocorreu erro ao efetuar o pagamento");
  };

  $scope.cancelPayment = function (payment) {
    doPost(paymentAPI.cancel, payment, "Pagamento cancelado com sucesso", "Ocorreu erro ao cancelar o pagamento");
  };
});