angular.module("craft-app").factory("timestampInterceptor",function () {
	return {
		request: function (config) {
			var url = config.url;
			if (url.indexOf("/api/") > -1) {
				if (url.indexOf("?")== -1){
					url+="?";
				} else {
					url+="&";
				}
				config.url = url+"timestamp="+new Date().getTime(); 
			}
			return config;
		}
	}
});