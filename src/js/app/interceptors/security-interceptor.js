angular.module("craft-app").factory("securityInterceptor",function ($q) {
	return {
		responseError: function (rejection) {
			console.log(">>>>>"+rejection.status);
			if (rejection.status == 401) {
				window.location = "/system?message";
				return;
			}
			return $q.reject(rejection);
		}
	}
});