var
gulp = require('gulp'),
rename = require('gulp-rename'),
replace = require('gulp-replace'),
minifyejs = require('gulp-minify-ejs'),
minifyCSS = require('gulp-minify-css'),
autoprefixer = require('gulp-autoprefixer'),
minifyHtml = require("gulp-minify-html"),
$ = require('gulp-load-plugins')(),
bust = new $.bust(),
ngmin = require('gulp-ngmin'),
watch = require('gulp-watch')
JS_VENDOR_FILENAME = 'jsdependencies.js',
sitejs = [
 "./bower_components/jquery/dist/jquery.min.js",
"./bower_components/materialize/dist/js/materialize.min.js"
],
sitecss=[
"./bower_components/materialize/dist/css/materialize.min.css",
"./src/css/style.css"
],
js = [
 "./bower_components/jquery/dist/jquery.min.js",
"./bower_components/jquery-ui/jquery-ui.min.js",
"./bower_components/jquery-ui/ui/i18n/datepicker-pt-BR.js",
"./bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js",

"./bower_components/materialize/dist/js/materialize.min.js",

"./bower_components/moment/min/moment-with-locales.min.js",
"./bower_components/moment/min/moment.min.js",
"./bower_components/moment-duration-format/lib/moment-duration-format.js",
"./bower_components/Chart.js/Chart.min.js",
"./bower_components/intro.js/intro.js",

// Angular dependencies
"./bower_components/angular/angular.min.js",
"./bower_components/angular-route/angular-route.min.js",
"./bower_components/angular-i18n/angular-locale_pt-br.js",
"./bower_components/angular-touch/angular-touch.min.js",
"./bower_components/angucomplete-alt/angucomplete-alt.js",
"./bower_components/angular-chart.js/dist/angular-chart.min.js"

],
css = [
"./bower_components/materialize/dist/css/materialize.min.css",
"./bower_components/jquery-ui/themes/ui-lightness/jquery-ui.min.css",
"./bower_components/angucomplete-alt/angucomplete-alt.css",
"./bower_components/angular-chart.js/dist/angular-chart.css",
"./bower_components/jquery-timepicker-jt/jquery.timepicker.css",
"./bower_components/intro.js/introjs.css",
"./src/css/style.css"
],
appjs=[
"./src/js/app/app.js",
"./src/js/app/home/home.js",
"./src/js/app/preferences/preferences-service.js",
"./src/js/app/preferences/preferences-controller.js",
"./src/js/app/fixed-costs/fixed-costs-service.js",
"./src/js/app/fixed-costs/fixed-costs-controller.js",
"./src/js/app/fixed-costs/fixed-costs-form-controller.js",
"./src/js/app/depreciation-costs/depreciation-costs-service.js",
"./src/js/app/depreciation-costs/depreciation-costs-controller.js",
"./src/js/app/depreciation-costs/depreciation-costs-form-controller.js",
"./src/js/app/stuff/stuff-service.js",
"./src/js/app/stuff/stuff-controller.js",
"./src/js/app/stuff/stuff-form-controller.js",
"./src/js/app/seller/seller-service.js",
"./src/js/app/seller/seller-controller.js",
"./src/js/app/seller/seller-form-controller.js",
"./src/js/app/customer/customer-service.js",
"./src/js/app/customer/customer-controller.js",
"./src/js/app/customer/customer-form-controller.js",
"./src/js/app/product/product-service.js",
"./src/js/app/product/product-controller.js",
"./src/js/app/product/product-form-controller.js",

"./src/js/app/signature/signature-service.js",

"./src/js/app/sale/sale-service.js",
"./src/js/app/sale/sale-controller.js",
"./src/js/app/sale/sale-form-controller.js",
"./src/js/app/sale/sale-service.js",
"./src/js/app/payment/payment-service.js",
"./src/js/app/config/interceptors-config.js",
"./src/js/app/config/routes.js",
"./src/js/app/config/routes-stuffs.js",
"./src/js/app/config/routes-seller.js",
"./src/js/app/config/routes-customer.js",
"./src/js/app/config/routes-product.js",
"./src/js/app/config/routes-sale.js",
"./src/js/app/util/util.js",
"./src/js/app/interceptors/timestamp-interceptor.js",
"./src/js/app/interceptors/security-interceptor.js",
"./src/js/app/users/user-service.js",
"./src/js/app/users/user-controller.js",
"./src/js/app/directives/file-model.js",
"./src/js/app/directives/ui-test.js"
];
gulp.task('site-js', function () {
  return gulp.src(sitejs)
  .pipe($.concat("js"))
  .pipe(rename('site.min.js'))
  .pipe($.uglify({mangle: false}))
  .pipe(gulp.dest('./public/js/'));
});
gulp.task('js', function () {
return gulp.src(js)
  .pipe($.concat("js"))
  .pipe(rename('js.min.js'))
  .pipe($.uglify({mangle: false}))
  .pipe(gulp.dest('./public/js/'));
});

gulp.task('html', function () {
return gulp.src('./src/html/**/**/*.html') // path to your files
  .pipe(minifyHtml())
  .pipe(gulp.dest('./public/templates/'));
});

gulp.task('ejs', function () {
return gulp.src('./src/views/**/**/*.ejs') // path to your files
  .pipe(minifyejs())
  .pipe(gulp.dest('./views/'));
});

gulp.task('html-dev', function () {
return gulp.src('./src/html/**/**/*.html') // path to your files       
  .pipe(gulp.dest('./public/templates/'));
});

gulp.task('appjs', function () {
return gulp.src(appjs)
  .pipe($.concat("appjs"))
  .pipe(ngmin())
  .pipe(rename('app.min.js'))
  .pipe($.uglify({mangle: false}))
  .pipe(gulp.dest('./public/js/'));
});

gulp.task('appjs-dev', function () {
return gulp.src(appjs)
  .pipe($.concat("appjs"))
  .pipe(rename('app.min.js'))
  .pipe(gulp.dest('./public/js/'));
});

gulp.task('css', function () {
return gulp.src(css)
  .pipe(minifyCSS())
  .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
  .pipe($.concat('style.min.css'))
  .pipe(gulp.dest('./public/css'))
});

gulp.task('sitecss', function () {
return gulp.src(sitecss)
  .pipe(minifyCSS())
  .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
  .pipe($.concat('style-site.min.css'))
  .pipe(gulp.dest('./public/css'))
});



gulp.task('build', ['js','site-js', 'appjs', 'css', 'sitecss', 'ejs', 'html']);
gulp.task("default", ['watch']);

gulp.task('watch', function() {
  gulp.watch('./src/views/**/**/*.ejs', ['ejs']);
  gulp.watch(css, ['css', 'sitecss']);
  gulp.watch('./src/html/**/**/*.html', ['html'])
  gulp.watch(appjs, ['appjs'])
});