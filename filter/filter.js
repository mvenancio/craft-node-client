var interceptUrl = function (url) {
  var startsWith = ["/font", "/css", "/js", "/images", 
  "/components", "/favicon", "/authorize", 
  "/register", "/system", "/api/users",
   "/forgot","/update-password", "/reset-password"],
      equals = ["/", "/login", "/new-user"],
      intercept = true;

  if (!intercept) {
    return false;
  }
  
  for (var i = 0; i < equals.length; i++) {
    if (url == equals[i]) {
      return false;
    }
  }

  for (var i = 0; i < startsWith.length; i++) {
    if (url.startsWith(startsWith[i])) {
      return false;
    }
  }
  return true;
};

module.exports = function(req, res, next) {
  if ( interceptUrl(req.originalUrl) ) {
    if (!req.session.authenticatedUser) {
      res.statusCode = 401;
      res.send("");
      return;
      // req.session.authenticatedUser = {
      //   "id":1,
      //   "email":"mateusvenan@gmail.com",
      //   "token":"b8f63e90e051c0a5440859106c1db30f4332ba674eb961328ccb6572890170be"
      // };

    }
  }
  next();  // call next() here to move on to next middleware/router
};