var
  express = require('express'),
  load = require('express-load'),
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  session = require('express-session'),
  RedisStore = require('connect-redis')(session),
  app = express(),
  compression = require('compression');
  filter = require('./filter/filter'),
  router = express.Router({ mergeParams : true }),
  fileUpload = require('express-fileupload');;

  app.use(router);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(cookieParser());
  app.use(session({
    name: "session",
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: false,
    rolling: true,
    store: new RedisStore({
      host: "localhost",
      port: 6379
    }),
    cookie:{ maxAge: 10080000 }
  }));
  app.use(compression());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(fileUpload());


  //app.set('PORT','8090');
  app.set('HOST','localhost');
  app.set('PORT','8080');                                                  
  // app.set('HOST','ec2-54-187-109-65.us-west-2.compute.amazonaws.com');                                                                 

  app.set('HEADERS',{
    "Authorization": "Basic "+ new Buffer("admin:admin").toString("base64"),
    "Content-type": "application/json",
    "Accept-Charset": "UTF-8"
  });
  app.set('HTTP_OPTS', {
    host: app.get('HOST'),
    port: app.get('PORT'),
    headers: app.get('HEADERS')
  })

  
  app.all('/*', function(request, response, next) {
    filter(request, response, next);
  });
  console.log(process.cwd());
  load('models')
    .then('controllers')
    .then('routes')
    .into(app);

  app.use(express.static(__dirname + '/public'));

  app.listen(process.env.PORT || 3000, function(){
    console.log("Aplicação iniciada");
  });