var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var productController = {

    get: function(request, response) {
      var
      params = request.params.id ?"/"+request.params.id : "",
      options = _.assign(app.get('HTTP_OPTS'),{path:"/products"+params, method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    post: function(request, response) {
      var 
      product = request.body,
      dataString = JSON.stringify(product),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/products", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    put: function(request, response) {
      var 
      product = request.body,
      dataString = JSON.stringify(product),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/products/" + product.id , method: "PUT", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    }

  };
  return productController;
};