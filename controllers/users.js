var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var UserController = {
    index: function(req, res) {
      res.render("user/users");
    },
    newUser: function(req, res) {
      res.render("user/newUser");
    },
    updatePassword: function(request, response) {
      var 
      password = request.body.password,
      confirm = request.body.confirmPassword,
      token = request.body.token,
      options = _.assign(app.get('HTTP_OPTS'), { path: "/users/token?token="+token, method: "GET" }),
      errorMessage = "";

      if (!password) {
        errorMessage += "Digite a nova senha\n";
      }
      if (!confirm) {
        errorMessage += "Digite a confirmação da nova senha\n";
      }

      if (password != confirm) {
        errorMessage+= "As senhas precisam ser iguais. Digite novamente!"
      }
      if (errorMessage != "") {
        response.render("forgot", {
          token: token,
          errorMessage: errorMessage
        });
        return;
      }

      httpUtil({
        request: request, 
        response: response,
        options: options,
        onResponseEnd: (statusCode, headers, data) => {
          var 
          user = JSON.parse(data);
          body = _.assign(user, {password: password}),
          dataString = JSON.stringify(user),
          requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
          options = _.assign(options, { path: "/users/"+user.id+"/password", method: "PUT" });

          
          httpUtil({  
            request: request, 
            response: response,
            options: options,
            body: dataString,
            onResponseEnd: (statusCode, headers, seconddata) => {
              
              response.render("system", {
                successMessage: "Sua senha foi atualizada com sucesso!"
              });
            },
            onRequestError: (e) => {
              response.render("forgot", {
                token: token,
                errorMessage: "Algo inexperado aconteceu. Tente novamente mais tarde."
              });
            }
          });
        },
        onRequestError: (e) => {
          response.render("forgot", {
            token: token,
            errorMessage: "Algo inexperado aconteceu. Tente novamente mais tarde."
          });
        }
      });
    },

    forgot: function(request, response) {
      response.render("forgot", {
        token: request.query.token
      })
    },

    resetPassword: function(request, response) {
      var 
      options = _.assign(options, app.get('HTTP_OPTS'),  { path: "/users/forgot-password?email="+request.body.email, method: "GET"});
      console.log("EMAIL >> "+request.body.email);
      if (!request.body.email) {
        response.render("system", {
          errorMessage: "Preencha o email"
        });
        return;
      }

      
      httpUtil({  
        request: request, 
        response: response,
        options: options,
        onResponseEnd: (statusCode, headers, seconddata) => {
          console.log(seconddata);
          response.render("system", {
            successMessage: "Foi enviado um email com o link para você trocar a senha!"
          });
        },
        onRequestError: (e) => {
          console.log(e);
          response.render("system", {
            errorMessage: "Email não encontrado."
          });
        }
      });
    },

    authorize: function(request, response) {
      var
      options = _.assign({ path: "/users/authorization?token="+request.query["token"], method: "GET" }, app.get('HTTP_OPTS'));

      httpUtil({
        request: request, 
        response: response,
        options: options,
        onResponseEnd: (statusCode, headers, data) => {
          var message = {};

          if (statusCode == 200) {
          	message.successMessage = "Parabéns! Seu usuário foi ativado com sucesso! Faça seu login e comece a utilizar imediatamente";
          } else {
          	message.errorMessage = "Algo inexperado aconteceu. Tente novamente mais tarde.";
          	console.log("WRONG EXPECTED STATUS CODE: "+statusCode+" RESPONSE: "+data);
          }
          response.statusCode = statusCode;
          response.headers = headers;

          response.render("system", message);
        },
        onRequestError: (e) => {
          response.statusCode = 500;
          response.send("unexpected error");
        }
      });

    }

  };
  return UserController;
};