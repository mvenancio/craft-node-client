var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util'),
    fs = require('fs'),
    easyimg = require('easyimage');;

module.exports = function(app) {
  var PreferencesController = {
    getByUser: function(request, response) {
      var
      options = _.assign( app.get('HTTP_OPTS'), {
        path:"/preferences/user", 
        method: "GET"
      });
      httpUtil({
        request: request,
        response: response,
        options: options,
        onResponseEnd: (statusCode, headers, data) => {
          var preferences = JSON.parse(data);
          request.session.preferences = preferences;
          request.session.save();
        }
      });
    },

    upload: function(req, res) {
      var
      dir = process.cwd()+"/public/user/"+req.session.authenticatedUser.id,
      dataString = JSON.stringify({
        path: ""
      }),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) });
      options = _.assign( app.get('HTTP_OPTS'), {
        path:"/craft-images", 
        method: "POST",
        headers: requestHeaders
      });;
         
      if (!req.files) {
        res.send('No files were uploaded.');
        return;
      }
      if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
      } 
      httpUtil({
        request: req,
        response: res,
        options: options,
        body: dataString,
        onResponseEnd: (statusCode, headers, data) => {
          var img = JSON.parse(data),
            sampleFile = req.files.sampleFile, 
            name = sampleFile.name,
            suffix = name.substring(name.length-4, name.length),
            imagePath = dir+'/'+img.id+suffix,
            imageMiniPath = dir+'/'+img.id+'-min'+suffix;

            console.log(suffix);

            sampleFile.mv(imagePath, function(err) {
              if (err) {
                res.status(500).send(err);
              }else {
                img.path = imagePath;
                var dataString = JSON.stringify(img),
                requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) });
                options.method = "PUT";
                options.headers = requestHeaders;

                 easyimg.resize({
                    src: imagePath, 
                    dst: imageMiniPath,
                    width:135, 
                    height:102
                  }).then(function(image) {
                     console.log('Resized : ' + image.width + ' x ' + image.height);
                  }, function (err) {
                    console.log(err);
                  }
                );
                 httpUtil({
                  request: req,
                  response: res,
                  options: options,
                  body: dataString,
                  onResponseEnd: (statusCode, headers, data) => {
                    res.send(data);
                  }
                });
              }
          });
        }
      });
    },

    get: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'),{
        path:"/preferences/user", 
        method: "GET"
      });
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    post: function(request, response) {
      var 
      preferences = request.body,
      dataString = JSON.stringify(preferences),
      requestHeaders = _.assign(app.get('HEADERS'),{ 
        "Content-Length": Buffer.byteLength(dataString) 
      }),
      options = _.assign(app.get('HTTP_OPTS'),{ 
        path: "/preferences", 
        method: "POST", 
        headers: requestHeaders 
      });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    put: function(request, response) {
      var 
      preferences = request.body,
      dataString = JSON.stringify(preferences),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'),{ 
        path: "/preferences/" + preferences.id , 
        method: "PUT",
        headers: requestHeaders
      });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    }
  };
  return PreferencesController;
};