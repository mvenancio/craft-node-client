var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var UserController = {

    get: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'), { path: "/users", method: "GET" });

      httpUtil({
        request: request,
        response: response,
        options: options
      });

    },

    registerByFacebook: function(request, response) {
      var
      options = _.assign({ path: "https://graph.facebook.com/me?fields=id,name,email,link,gender&access_token=", method: "GET" }, app.get('HTTP_OPTS'));

      httpUtil({
        request: request,
        response: response,
        options: options
      });

    },

    post: function(request, response) {
      var
      user = request.body,
      dataString = JSON.stringify(user),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/users", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    put: function(request, response) {
      var 
      user = request.body,
      dataString = user.email,
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/users/" + user.id + "/email", method: "PUT", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    sendEmail: function (request, response) {
      var 
      requestHeaders = app.get('HEADERS'),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/users/resend-authorization-mail?email="+request.query.email, method: "GET", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    }
  };
  return UserController;
};