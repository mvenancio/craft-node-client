var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var saleController = {

    get: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'), {path:"/sales", method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },
    getBy: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'),{path:"/sales/"+request.params.id, method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    }, 

    getFinishedsSummary: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'),{path:"/sales/finisheds/summary", method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    post: function(request, response) {
      var
      sale = request.body,
      dataString = JSON.stringify(sale),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/sales", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    put: function(request, response) {
      var 
      sale = request.body,
      dataString = JSON.stringify(sale),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/sales/" + sale.id , method: "PUT", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    approve: function(request, response) {
      var 
      sale = request.body,
      requestHeaders = app.get('HEADERS'),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/sales/" + sale.id +"/approve", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    getPaymentsFromSale: function(request, response) {
      var 
      requestHeaders = app.get('HEADERS'),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/sales/" + request.params.id +"/payments", method: "GET", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    cancel: function(request, response) {
      var 
      sale = request.body,
      requestHeaders = app.get('HEADERS'),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/sales/" + sale.id +"/cancel", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    }

  };
  return saleController;
};