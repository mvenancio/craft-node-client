var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var SellerController = {

    get: function(request, response) {
      var
      params = request.params.id ? "/" + request.params.id : "",
      options = _.assign(app.get('HTTP_OPTS'), {path:"/sellers"+params, method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    post: function(request, response) {
      var 
      seller = request.body,
      dataString = JSON.stringify(seller),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/sellers", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: JSON.stringify(seller)
      });
    },

    put: function(request, response) {
      var 
      seller = request.body,
      dataString = JSON.stringify(seller),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/sellers/" + seller.id , method: "PUT", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    }

  };
  return SellerController;
};