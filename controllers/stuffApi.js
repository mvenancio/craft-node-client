var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var StuffController = {

    get: function(request, response) {
      var
      params = request.params.id ? "/"+request.params.id : "",
      options = _.assign(app.get('HTTP_OPTS'), {path:"/stuffs"+params, method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    post: function(request, response) {
      var 
      stuff = request.body,
      dataString = JSON.stringify(stuff),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/stuffs", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    put: function(request, response) {
      var 
      stuff = request.body,
      dataString = JSON.stringify(stuff),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/stuffs/" + stuff.id , method: "PUT", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    }

  };
  return StuffController;
};