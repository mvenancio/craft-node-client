var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  return {

    get: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'), { path:"/signatures", method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    post: function(request, response) {
      var 
      options = _.assign(app.get('HTTP_OPTS'), { path: "/signatures", method: "POST"});

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },
    
    delete: function(request, response) {
      var 
      options = _.assign(app.get('HTTP_OPTS'), { path: "/signatures", method: "DELETE"});

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    }
  };
};