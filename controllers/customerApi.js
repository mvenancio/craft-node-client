var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var CustomerController = {

    get: function(request, response) {
      var
      params = request.params.id ? "/" + request.params.id : "",
      options = _.assign(app.get('HTTP_OPTS'), {path:"/customers"+params, method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    post: function(request, response) {
      var 
      customer = request.body,
      dataString = JSON.stringify(customer),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/customers", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: JSON.stringify(customer)
      });
    },

    put: function(request, response) {
      var 
      customer = request.body,
      dataString = JSON.stringify(customer),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), {path: "/customers/" + customer.id , method: "PUT", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    }

  };
  return CustomerController;
};