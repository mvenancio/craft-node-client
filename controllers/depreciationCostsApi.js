var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var DepreciationCostsController = {

    get: function(request, response) {
      var
      params = request.params.id ? "/"+request.params.id : "", 
      options = _.assign(app.get('HTTP_OPTS'), {path:"/depreciation-costs"+params, method: "GET"});
      httpUtil({
        request: request,
        response: response,
        options: options
      });

    },

    getTotal: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'), {path:"/depreciation-costs/total", method: "GET"});


      httpUtil({
        request: request,
        response: response,
        options: options
      });

    },

    post: function(request, response) {
      var 
      fixedCost = request.body,
      dataString = JSON.stringify(fixedCost),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/depreciation-costs", method: "POST", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    },

    put: function(request, response) {
      var 
      fixedCost = request.body,
      dataString = JSON.stringify(fixedCost),
      requestHeaders = _.assign(app.get('HEADERS'), { "Content-Length": Buffer.byteLength(dataString) }),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/depreciation-costs/" + fixedCost.id , method: "PUT", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options,
        body: dataString
      });
    }

  };
  return DepreciationCostsController;
};