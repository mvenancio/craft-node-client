var http = require('http'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var paymentController = {

    get: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'), {path:"/payments", method: "GET"});
      
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    getExpired: function(request, response) {
      var
      options = _.assign(app.get('HTTP_OPTS'), {path:"/payments/expired", method: "GET"});
      
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    pay: function(request, response) {
      var 
      payment = request.body,
      requestHeaders = app.get('HEADERS'),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/payments/" + payment.id +"/pay", method: "POST",  headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },

    cancel: function(request, response) {
      var 
      payment = request.body,
      requestHeaders = app.get('HEADERS'),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/payments/" + payment.id +"/cancel", method: "POST", headers: requestHeaders });
      console.log(options);
      httpUtil({
        request: request,
        response: response,
        options: options
      });
    },


    totalPaid: function(request, response) {
      var 
      requestHeaders = app.get('HEADERS'),
      options = _.assign(app.get('HTTP_OPTS'), { path: "/payments/total-paid", method: "GET", headers: requestHeaders });

      httpUtil({
        request: request,
        response: response,
        options: options
      });
    }

  };
  return paymentController;
};