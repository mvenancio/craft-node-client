var http = require('http'),
    async = require('async'),
    _ = require('lodash'),
    httpUtil = require('../util/http-util');

module.exports = function(app) {
  var HomeController = {
    index: function(req, res) {
      if (req.session.authenticatedUser) {
        res.redirect('/main?showIntro=true');
      } else {
        res.render("index");
      }
    },
    system: function(req, res) {
      if (req.session.authenticatedUser) {
        res.redirect('/main?showIntro=true');
      } else {
        res.render("system");
      }
    },
    register: function(request, response) {
      response.render("register");
    },
    home: function(req, res) {
      res.render("home",{
        showIntro: req.query.showIntro
      });
    },
    preferences: function(req, res) {
      res.render("preferences");
    },
    logout: function(request, response) {
      delete request.session.authenticatedUser;
      request.session.destroy();
      response.redirect('/');
    },
    login: function(request, response) {
      var email = request.body.email,
          password = request.body.password;
      if(email && password) {
        var
        body = {
          email: email,
          password: password
        },
        bodyStr = JSON.stringify(body),
        headers = _.assign(app.get('HEADERS'), {"Content-Length": bodyStr.length}),
        options = _.assign(app.get('HTTP_OPTS'), { "path": "/users/authenticate", "method": "POST", "headers": headers});

        httpUtil({
          request: request,
          response: response,
          options: options,
          onResponseEnd: (statusCode, headers, data, request, response) => {
             if (statusCode == 200) {
              var user = JSON.parse(data);
              request.session.authenticatedUser = user;
              request.session.save();

              app.controllers.preferencesApi.getByUser(request);

              response.statusCode = statusCode;
              response.headers = headers;
              response.redirect('/main?showIntro=true');
            } else {
              response.render("system", {
                errorMessage: "Login e/ou senha inválidos."
              });
            }
          },
          onRequestError: (e, request, response) => {
            response.render('system', {
              errorMessage: "Parece que tem algum problema em nosso sistema :(..  Aguarde alguns minutos que já estamos verificando" 
            });
          },
          body: bodyStr
        });
      } else {
        response.render('/system', {
          errorMessage: "Email e senha são campos obrigatórios." 
        });
      }
    }
  };
  return HomeController;
};