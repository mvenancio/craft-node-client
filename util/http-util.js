var 
http = require('http'),
_ = require('lodash'),
_onRequestError = (response, e) => {
  console.log("ERROR >>> "+ e);
  response.statusCode = 500;
  response.send("unexpected error");
},
_onResponseEnd = (response, statusCode, headers, data) => {
  response.statusCode = statusCode;
  response.headers = headers;
  response.send(data ? data : []);
};

module.exports = (call) => {
  if (call.request.session.authenticatedUser) {
    var headers = _.assign({"X-usr-id":call.request.session.authenticatedUser.token}, call.options.headers);
    call.options.headers = headers;
  }
  
  var req = http.request(call.options, (res) => {

    //console.log(">>HEADERS: "+JSON.stringify(res.headers));
    var data = '';
    res.on('data', (d) => {
      data+=d;
    });
    res.on('end', () => {
      call.onResponseEnd ? call.onResponseEnd(res.statusCode, res.headers, data, call.request, call.response) : _onResponseEnd(call.response, res.statusCode, res.headers, data);
    });
  });

  req.on('socket', (socket) => {
      socket.setTimeout(call.ttl||60000);
      socket.on('timeout', function() {
        req.abort();
      });
  });

  if ((call.options.method == "POST" || call.options.method == "PUT") && call.body) {
    req.write(new Buffer(call.body), encoding='utf8');
  }
  req.end();

  req.on('error', (e) => {
    call.onRequestError ? call.onRequestError(e, call.request, call.response) : _onRequestError(call.response, e);
  });
}