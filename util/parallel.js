var request = require('request')

var api = function(config, req){
	var baseApi = request.defaults({
		baseUrl: "http://srartesao.com.br:8080", 
		headers: config.headers
	});

	//funcao para fazer get no guiche e colocar num model
	baseApi.getAsync = function(url, qs){
	  return function (callback) {
	      baseApi.get({
	      		uri: url, 
	      		qs: qs 
	      	}, function (error, incoming, body) {
		      	if(error){
		      		console.warn('Falha ao chamar api',error);
		      	}
		      	callback(error,body);
	      });
	  };
  	};
  	return baseApi;
};

module.exports = api;